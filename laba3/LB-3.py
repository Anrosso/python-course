def segments(a,b,c,d):
  if min(a[0],b[0])<=max(c[0],d[0]) and min(c[1],d[1])<=max(a[1],b[1]) and min(c[0],d[0])<=max(a[0],b[0]) and min(a[1],b[1])<=max(c[1],d[1]):
    return 'Заданные отрезки пересекаются'
  else:
    return 'Заданные отрезки не пересекаются'

x1, y1 = map(int, input('Введите значения X и Y точки a через пробел: ').split())
x2, y2 = map(int, input('Введите значения X и Y точки b через пробел: ').split())
x3, y3 = map(int, input('Введите значения X и Y точки c через пробел: ').split())
x4, y4 = map(int, input('Введите значения X и Y точки d через пробел: ').split())

a = [x1, y1]
b = [x2, y2]
c = [x3, y3]
d = [x4, y4]

res = segments(a,b,c,d)
print(res)

circle = True
login = input('Введите имя пользователя: ')
while circle == True:
    password = input('Введите пароль: ')
    if password == '1111_p':
        print(login + ' - Ваш пароль правильный')
        break
    elif password == 'No':
        break
    else:
        print(login + ' - Ваш пароль не правильный! Введите еще раз.')

print('Done')