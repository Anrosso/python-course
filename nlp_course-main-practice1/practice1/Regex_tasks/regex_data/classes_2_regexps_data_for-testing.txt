-------------
Cinderella
-------------



Once upon a time, there lived an unhappy young girl. Unhappy she was, for her mother was dead, her father had married another woman, a widow with two daughters, and her stepmother didn’t like her one little bit. All the nice things, kind thoughts and loving touches were for her own daughters. And not just the kind thoughts and love, but also dresses, shoes, shawls, delicious food, comfy beds, as well as every home comfort. All this was laid on for her daughters. But, for the poor unhappy girl, there was nothing at all. No dresses, only her stepsisters’ hand-me-downs. No lovely dishes, nothing but scraps. No nice rests and comfort. For she had to work hard all day, and only when evening came was she allowed to sit for a while by the fire, near the cinders. That is how she got her nickname, for everybody called her Cinderella. Cinderella used to spend long hours all alone talking to the cats. The cat said,
Meow, which really meant, Cheer up! You have something neither of your stepsisters have and that is beauty.

It was quite true. Cinderella, even dressed in rags with a dusty grey face from the cinders, was a lovely girl. While her stepsisters, no matter how splendid and elegant their clothes, were still clumsy, lumpy and ugly, and they always would be.

One day, beautiful new dresses arrived at the house. A ball was to be held at Court and the stepsisters were getting ready to go to it. Cinderella, didn’t even dare ask, What about me? for she knew very well what the answer to what would be.

You? My dear girl, you’re staying at home to wash the dishes, scrub the floors and turn down the beds for your stepsisters. They will come home tired and very sleepy. Cinderella sighed at the cat.

Oh dear, I’m so unhappy! and the cat murmured meow.

Suddenly something amazing happened. In the kitchen, where Cinderella was sitting all by herself, there was a burst of light and a fairy appeared.

Don’t be alarmed, Cinderella, said the fairy. The wind blew me your sighs. I know you would love to go to the ball. And so you shall!

How can I, dressed in rags? Cinderella replied. The servants will turn me away! The fairy smiled. With a flick of her magic wand, Cinderella found herself wearing the most beautiful dress, the loveliest ever seen in the realm.

Cinderalla with her fairy godmother, by Edmund Dulac

Now that we have settled the matter of the dress, said the fairy, we’ll need to get you a coach. A real lady would never go to a ball on foot!

Quick! Get me a pumpkin! she ordered.

Oh of course, said Cinderella, rushing away. Then the fairy turned to the cat.

You, bring me seven mice!

Seven mice! said the cat. Oh, I didn’t know fairies ate mice too!

They’re not for eating, silly! Do as you are told, and remember they must be alive!

Cinderella soon returned with a fine pumpkin and the cat with seven mice he had caught in the cellar.

Good! exclaimed the fairy. With a flick of her magic wand — wonder of wonders! The pumpkin turned into a sparkling coach and the mice became six white horses, while the seventh mouse turned into a coachman, in a smart uniform and carrying a whip. Cinderella could hardly believe her eyes.

I shall present you at Court. You will soon see that the Prince, in whose honour the ball is being held, will not be enchanted by your loveliness. But remember! You must leave the ball at midnight and come home. For that is when the spell ends. Your coach will turn back into a pumpkin, the horses will become mice again and the coachman will turn back into a mouse, and you will be dressed again in rags and wearing clogs instead of these dainty little slippers! Do you understand?

Cinderella smiled and said, Yes, I understand!

When Cinderella entered the ballroom at the palace, a hush fell. Everyone stopped in mid-sentence to admire her elegance, her beauty and grace.

Who can that be? people asked each other. The two stepsisters also wondered who the newcomer was, for never in a month of Sundays, would they ever have guessed that the beautiful girl was really poor Cinderella who talked to the dogs!

When the prince set eyes on Cinderella, he was struck by her beauty. Walking over to her, he bowed deeply and asked her to dance. And to the great disappointment of all the young ladies, he danced with Cinderella all evening.

Who are you, fair maiden? the Prince kept asking her.

But Cinderella only replied: What does it matter who I am! You will never see me again anyway.

Oh, but I shall, I’m quite certain! he replied.

Cinderella looses her glass slipper, by Edmund Dulac

Cinderella had a wonderful time at the ball, but, all of a sudden, she heard the sound of a clock: the first stroke of midnight! She remembered what the fairy had said, and without a word of goobye she slipped from the Prince’s arms and ran down the steps. As she ran she lost one of her slippers, but not for a moment did she dream of stopping to pick it up! If the last stroke of midnight were to sound… oh, what a disaster that would be! Out she fled and vanished into the night.

The Prince, who was now madly in love with her, picked up her slipper and said to his ministers, Go and search everywhere for the girl whose foot this slipper fits. I will never be content until I find her! So the ministers tried the slipper on the foot of all the girls… and on Cinderella’s foot as well… Surprise! The slipper fit her perfectly.

That awful untidy girl simply cannot have been at the ball, snapped the stepmother. Tell the Prince he ought to marry one of my two daughters! Can’t you see how ugly Cinderella is! Can’t you see?

Suddenly she broke off, for the fairy had appeared.

That’s enough! she exclaimed, raising her magic wand. In a flash, Cinderella appeared in a splendid dress, shining with youth and beauty. Her stepmother and stepsisters gaped at her in amazement, and the ministers said,

Come with us, fair maiden! The Prince awaits to present you with his engagement ring! So Cinderella joyfully went with them, and lived happily ever after with her Prince.

And as for the cat, he just said meow!





-------------
Pinocchio
-------------


There was once upon a time a piece of wood in the shop of an old carpenter named Master Antonio. Everybody, however, called him Master Cherry, on account of the end of his nose, which was always as red and polished as a ripe cherry.

No sooner had Master Cherry set eyes on the piece of wood than his face beamed with delight, and, rubbing his hands together with satisfaction, he said softly to himself:

"This wood has come at the right moment; it will just do to make the leg of a little table."

He immediately took a sharp axe with which to remove the bark and the rough surface, but just as he was going to give the first stroke he heard a very small voice say imploringly, "Do not strike me so hard!"

He turned his terrified eyes all around the room to try and discover where the little voice could possibly have come from, but he saw nobody! He looked under the bench—nobody; he looked into a cupboard that was always shut—nobody; he looked into a basket of shavings and sawdust—nobody; he even opened the door of the shop and gave a glance into the street—and still nobody. Who, then, could it be?

"I see how it is," he said, laughing and scratching his wig, "evidently that little voice was all my imagination. Let us set to work again."

And, taking up the axe, he struck a tremendous blow on the piece of wood.

"Oh! oh! you have hurt me!" cried the same little voice dolefully.

This time Master Cherry was petrified. His eyes started out of his head with fright, his mouth remained open, and his tongue hung out almost to the end of his chin, like a mask on a fountain. As soon as he had recovered the use of his speech he began to say, stuttering and trembling with fear:

"But where on earth can that little voice have come from that said 'Oh! oh!'? Is it possible that this piece of wood can have learned to cry and to lament like a child? I cannot believe it. This piece of wood is nothing but a log for fuel like all the others, and thrown on the fire it would about suffice to boil a saucepan of beans. How then? Can anyone be hidden inside it? If anyone is hidden inside, so much the worse for him. I will settle him at once."

So saying, he seized the poor piece of wood and commenced beating it without mercy against the walls of the room.

Then he stopped to listen if he could hear any little voice lamenting. He waited two minutes—nothing; five minutes—nothing; ten minutes—still nothing!

"I see how it is," he then said, forcing himself to laugh, and pushing up his wig; "evidently the little voice that said 'Oh! oh!' was all my imagination! Let us set to work again."

Putting the axe aside, he took his plane, to plane and polish the bit of wood; but whilst he was running it up and down he heard the same little voice say, laughing:

"Stop! you are tickling me all over!"

This time poor Master Cherry fell down as if he had been struck by lightning. When he at last opened his eyes he found himself seated on the floor.

His face was changed, even the end of his nose, instead of being crimson, as it was nearly always, had become blue from fright.






------------- 
King Kong
-------------



With humor, rage, and confessional detail, Virginie Despentes in her own words "more king kong than Kate Moss"--delivers a highly charged account of women's lives today. She explodes common attitudes about sex and gender, and shows how modern beauty myths are ripe for rebelling against. Using her own experiences of rape, prostitution, and working in the porn industry as a jumpingoff point, she creates a new space for all those who can't or won't obey the rules.




------------
Aviation
------------




Here are the 10 worlds largest passenger jet aircraft that can transport hundreds of passengers from another side of the world within an hour. The largest one, Airbus A380 celebrates the anniversary of its first flight. Take a look at the list of the largest passenger aircraft in the world!

The Airbus A340 500 is a passenger airplane manufactured in France. It came out in 2006, though there were early deliveries in 2002. It is designed for 372 passengers in a single-class seat arrangement and 313 in a double class system.

The Boeing 77-72-77-72 is a passenger plane made in the United States. It can accommodate 440 passengers in a single-class system and 400 in a two-class configuration. It has a travel range of 14,260 kilometers or 7,700 nautical miles.

The Boeing 777 300 is a passenger plane manufactured in the United States with a seating capacity of 550 people in a single-class setup and 451 in a two-class configuration. It has a travel range of 6,013 nautical miles or 11,135 kilometers.

The Airbus A380 800 is a passenger plane made in France with capacity for 853 passengers in a single class or 644 in a two-tiered class. It has a travel range of 8,208 nautical miles or 15,200 kilometers. The plane costs $318 million. However, Airbus has just announced that it would retire the jet in 2021. The European manufacturer stated that with Emirates reducing its order, the program is not sustainable anymore.




------------- 
Names table
-------------

Sam is a manager.
She is a biologyst.
He is an actor.
Kate was a producer.
My mom is a doctor.
Bob is a director.



