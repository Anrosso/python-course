import math

consonants = 'bcdfghjklmnpqrstvwxyz'

def circle_area(radius):
    s = math.pi * radius ** 2
    return s

def string_change(str):
    # Получаем последний символ в строке
    symbol = str[-1]
    # Проверяем, не является ли последний символ символом переноса строки
    if ord(symbol) == 10:
        # Если да, то берем предпоследний символ с конца строки
        symbol = str[len(str) - 2:len(str) - 1:1]
        # Отсекаем последний символ строки (перенос строки)
        str = str[0:len(str) - 1]

    # Проходим по списку согласных
    for c in consonants:
        # Проверяем, является ли последний символ строки согласной буквой
        if c == symbol:
            str = str + symbol + 'ay'

    return str

r = int(input('Введите радиус круга: '))

S = circle_area(r)

print('Площадь круга с радиусом {0:d} равна {1:10.4f}'.\
      format(r, S))

n = 56
print('\nРадиус в восьмиричном представлении \t\t{0:o},\nв шестнадцатиричном представлении \t\t\t{0:x},\n\
в экспотенциальном представлении \t\t\t{0:10e},\nв десятичном представлении \t\t\t\t\t{0:d}'.\
format(n))

#
# Работа с файлами
#
fhand = open('file1.txt', 'w')
text1 = 'Thou blind fool, Love, what dost thou to mine eyes'
text2 = 'That they behold, and see not what they see'
text3 = 'They know what beauty is, see where it lies'
text4 = 'Yet what the best is take the worst to be'

fhand.write(text1 + '\n')
fhand.write(text2 + '\n')
fhand.write(text3 + '\n')
fhand.write(text4)

fhand.close()

fhand1 = open('file1.txt', "rt")
fhand2 = open('file2.txt', 'w')
for str in fhand1:
    fhand2.write(string_change(str) + '\n')
